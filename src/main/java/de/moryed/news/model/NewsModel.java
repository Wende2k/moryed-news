package de.moryed.news.model;

import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class NewsModel {

    private int id;
    private String message;

}
