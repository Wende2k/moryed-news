package de.moryed.news;

import com.google.common.collect.ImmutableMap;
import de.moryed.news.mysql.MysqlManager;
import de.moryed.news.task.BroadcastTask;
import de.moryed.news.util.Config;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.util.LinkedHashMap;

@Plugin(name = "Moryed-News", version = "1.0-SNAPSHOT")
@Author(name = "Wende2k")
@Description(desc = "Newsbar implementation for Moryed.de")
@Command(name = "news", permission = "moryed.news")
@Permission(name = "moryed.news", desc = "Allows the usage of News command", defaultValue = PermissionDefault.OP)
public class NewsPlugin extends JavaPlugin {

    private static final String PREFIX = "§bNews §7* §e";

    @Getter
    private static MysqlManager mysqlManager;

    private NewsManager newsManager;

    @Override
    public void onLoad() {
        Config config = new Config("plugins/Moryed-News", "config.yml");

        if (!config.contains("mysql")) {
            new LinkedHashMap<String, Object>() {{
                super.put("mysql.host", "127.0.0.1:3306");
                super.put("mysql.username", "root");
                super.put("mysql.password", "password");
                super.put("mysql.database", "database");
            }}.forEach(config::set);
            config.saveFile();
        }

        NewsPlugin.mysqlManager = new MysqlManager(config);
        this.newsManager = new NewsManager();
    }

    @Override
    public void onEnable() {
        super.getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onInventoryClick(InventoryClickEvent event) {
                if (event.getInventory().equals(NewsPlugin.this.newsManager.getInventory())) {
                    event.setCancelled(true);

                    ItemStack itemStack = event.getCurrentItem();

                    if (itemStack != null && itemStack.getType() != Material.AIR) {
                        NewsPlugin.this.newsManager.remove(itemStack);
                    }
                }
            }
        }, this);

        super.getCommand("news").setExecutor((commandSender, command, label, args) -> {
            if (args.length > 0) {
                this.newsManager.add(StringUtils.join(args, " "));
                commandSender.sendMessage(PREFIX + "§aDeine News wurden erfolgreich hinzugefügt!");
            } else {
                if (commandSender instanceof Player) {
                    ((Player) commandSender).openInventory(this.newsManager.getInventory());
                }
            }

            return true;
        });

        new BroadcastTask(this.newsManager.getNewsModels()).runTaskTimer(this, 10L, 10L);
    }

    @Override
    public void onDisable() {
        NewsPlugin.mysqlManager.closeConnection();
    }

}
