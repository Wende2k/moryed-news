package de.moryed.news;

import de.moryed.news.model.NewsModel;
import de.moryed.news.mysql.MysqlManager;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public class NewsManager {

    private static final String TABLE_NAME = "`news_messages`";

    private List<NewsModel> newsModels = new ArrayList<>();
    private MysqlManager mysqlManager = NewsPlugin.getMysqlManager();

    private Inventory inventory = Bukkit.createInventory(null, 5 * 9, "§0§lNews Messages");

    {
        this.mysqlManager.queryUpdate("CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " (`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT, `message` VARCHAR(1500))");
        this.load();
        this.updateInventory();
    }

    @SneakyThrows
    public void load() {
        ResultSet resultSet = this.mysqlManager.query("SELECT * FROM " + TABLE_NAME);

        while (resultSet.next()) {
            this.newsModels.add(
                    new NewsModel(resultSet.getInt("id"), resultSet.getString("message"))
            );
        }
    }

    @SneakyThrows
    public void add(String message) {
        String translated = ChatColor.translateAlternateColorCodes('&', message);
        ResultSet resultSet = this.mysqlManager.queryUpdateResult(
                this.mysqlManager.prepareStatement("INSERT INTO " + TABLE_NAME + " (`message`) VALUES ('" +
                                translated + "')",
                Statement.RETURN_GENERATED_KEYS));

        while (resultSet.next()) {
            this.newsModels.add(new NewsModel(resultSet.getInt(1), translated));
        }

        this.updateInventory();
    }

    public void remove(ItemStack itemStack) {
        if (itemStack.hasItemMeta()) {
            ItemMeta itemMeta = itemStack.getItemMeta();

            if (itemMeta.hasLore()) {
                int id = Integer.valueOf(itemMeta.getLore().get(0).split(" ")[1]);

                this.newsModels.removeIf(newsModel ->
                    newsModel.getId() == id
                );

                this.mysqlManager.queryUpdate("DELETE FROM " + TABLE_NAME + " WHERE `id`=" + id);
            }
        }

        this.updateInventory();
    }

    public void updateInventory() {
        this.inventory.clear();

        this.newsModels.forEach(newsModel -> {
            ItemStack itemStack = new ItemStack(Material.PAPER);
            ItemMeta itemMeta = itemStack.getItemMeta();

            itemMeta.setDisplayName(newsModel.getMessage());
            itemMeta.setLore(Arrays.asList("§7ID: " + newsModel.getId(),
                    "§cKlicke, um diese Nachricht zu entfernen.",
                    "§aBenutze /news <Nachricht> für das Hinzufügen."));

            itemStack.setItemMeta(itemMeta);

            this.inventory.addItem(itemStack);
        });

        this.inventory.getViewers().forEach(humanEntity ->
            humanEntity.openInventory(this.inventory)
        );
    }

}
