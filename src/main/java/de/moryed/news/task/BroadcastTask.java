package de.moryed.news.task;

import de.moryed.news.model.NewsModel;
import lombok.RequiredArgsConstructor;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

@RequiredArgsConstructor
public class BroadcastTask extends BukkitRunnable {

    private final List<NewsModel> newsModels;
    private int index = 0;
    private int count = 0;

    @Override
    public void run() {
        if (!newsModels.isEmpty()) {
            if (index == newsModels.size()) {
                index = 0;
            }

            Bukkit.getOnlinePlayers().forEach(player ->
                    this.send(newsModels.get(index).getMessage(), player)
            );

            if (count >= 20) {
                index++;
                count = 0;
            } else {
                count++;
            }
        }
    }

    public void send(String message, Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(
                IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + message + "\"}"), (byte) 2));
    }

}
